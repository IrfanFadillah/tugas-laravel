<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
  
    <form action="/welcome" method="POST">
    @csrf   
    <label>First name:</label><br><br>
    <input type="text" name="first_name"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="last_name"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio" name="Gender" value="0">Male<br>
    <input type="radio" name="Gender" value="1">Female<br>
    <input type="radio" name="Gender" value="2">Other<br><br>

    <label>Nationality:</label><br><br>
    <select name="negara" id="">
        <option value="">Indonesia</option>
        <option value="">Singapure</option>
        <option value="">Malaysia</option>
        <option value="">Amerika</option>
    </select>

    <br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>

    <label>Bio:</label><br><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up" >
</form>
</body>
</html>