<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Home');
});

// Route::get('/register/{angka}', function($angka){
//     return view('Register', ["angka" => $angka]);
// })

// Route::get('/welcome/{nama}', function($nama){
//     return view('Welcome', ["nama" => $nama]);
// })

Route::get('/home', 'HomeController@home');
Route::get('/home_post', 'HomeController@register');

Route::get('/form', 'AuthController@form');

Route::get('/welcome', 'AuthController@welcome');
Route::post('/welcome', 'AuthController@welcome_post');